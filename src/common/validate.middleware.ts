import { type Request, type Response, type NextFunction } from 'express';
import { type ParamsDictionary } from 'express-serve-static-core';
import { type ParsedQs } from 'qs';
import { type IMiddleWare } from './middleware.interface';
import { plainToClass, type ClassConstructor } from 'class-transformer';
import { validate } from 'class-validator';

export class ValidateMiddleWare implements IMiddleWare {
	constructor(private readonly classToValidate: ClassConstructor<object>) {}

	execute(
		{ body }: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction,
	): void {
		const instance = plainToClass(this.classToValidate, body);

		validate(instance).then((errors) => {
			if (errors.length > 0) {
				res.status(422).send(errors);
			} else {
				next();
			}
		});
	}
}

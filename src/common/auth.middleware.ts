import { type NextFunction, type Request, type Response } from 'express';
import { type IMiddleWare } from './middleware.interface';
import { type JwtPayload, verify } from 'jsonwebtoken';

export class AuthMiddleWare implements IMiddleWare {
	constructor(private readonly secret: string) {}

	execute(req: Request, res: Response, next: NextFunction): void {
		if (req.headers.authorization) {
			// Bearer JWT

			verify(req.headers.authorization.split(' ')[1], this.secret, (err, payload) => {
				if (err) {
					next();
				} else if (payload) {
					req.user = (payload as JwtPayload).email;
					next();
				}
			});
		} else {
			next();
		}
	}
}

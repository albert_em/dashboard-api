import { Router, type Response } from 'express';
import 'reflect-metadata';
import { injectable } from 'inversify';

import { type ExpressReturnType, type IControllerRoute } from './route.interface';
import { ILogger } from '../logger/logger.interface';
@injectable()
export abstract class BaseController {
	private readonly _router: Router;

	constructor(private readonly logger: ILogger) {
		this._router = Router();
	}

	get router(): Router {
		return this._router;
	}

	public send<T>(res: Response, code: number, message: T): ExpressReturnType {
		res.type('application/json');
		return res.status(code).json(message);
	}

	public ok<T>(res: Response, message: T): void {
		this.send<T>(res, 200, message);
	}

	public created(res: Response): ExpressReturnType {
		return res.sendStatus(201);
	}

	protected bindRoutes(routes: IControllerRoute[]): void {
		for (const route of routes) {
			const { method, path, func, middlewares } = route;

			this.logger.log(`[${method}] ${path}`);
			const middleware = middlewares?.map((m) => m.execute.bind(m));
			const handler = func.bind(this);
			const pipeline = middleware ? [...middleware, handler] : handler;
			this.router[method](path, pipeline);
		}
	}
}

import { type Request, type Response, type NextFunction } from 'express';
import { type ParamsDictionary } from 'express-serve-static-core';
import { type ParsedQs } from 'qs';
import { type IMiddleWare } from './middleware.interface';

export class AuthGuard implements IMiddleWare {
	execute(
		req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>,
		res: Response<any, Record<string, any>>,
		next: NextFunction,
	): void {
		if (req.user) {
			next();
		} else {
			res.status(401).send({ error: 'You are not authorized' });
		}
	}
}

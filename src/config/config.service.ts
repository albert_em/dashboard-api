/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { inject, injectable } from 'inversify';
import { type IConfigService } from './config.service.interface';
import { type DotenvConfigOutput, config, type DotenvParseOutput } from 'dotenv';
import { TYPES } from '../types';
import { ILogger } from '../logger/logger.interface';

@injectable()
export class ConfigService implements IConfigService {
	private readonly config: DotenvParseOutput;
	constructor(@inject(TYPES.ILogger) private readonly logger: ILogger) {
		const result: DotenvConfigOutput = config();

		if (result.error) {
			this.logger.error('Failed trying to read .env config');
		} else {
			this.logger.log('[ConfigService] Configuration .env loaded');
			this.config = result.parsed!;
		}
	}

	get(key: string): string {
		return this.config[key];
	}
}
